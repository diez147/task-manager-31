package ru.tsc.babeshko.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.tsc.babeshko.tm.api.repository.ITaskRepository;
import ru.tsc.babeshko.tm.model.Task;

import java.util.List;
import java.util.stream.Collectors;

public class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @NotNull
    @Override
    public Task create(@NotNull final String userId, @NotNull final String name) {
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setUserId(userId);
        return add(task);
    }

    @NotNull
    @Override
    public Task create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description) {
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setUserId(userId);
        task.setDescription(description);
        return add(task);
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        return records.stream()
                .filter(task -> userId.equals(task.getUserId()) && projectId.equals(task.getProjectId()))
                .collect(Collectors.toList());
    }

}