package ru.tsc.babeshko.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.babeshko.tm.enumerated.Role;
import ru.tsc.babeshko.tm.model.User;

public interface IAuthService {

    void registry(@NotNull String login, @NotNull String password, @Nullable String email);

    void login(@NotNull String login, @NotNull String password);

    void logout();

    boolean isAuth();

    @NotNull
    String getUserId();

    @NotNull
    User getUser();

    void checkRoles(@Nullable Role[] roles);

}