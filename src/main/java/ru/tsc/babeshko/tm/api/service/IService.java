package ru.tsc.babeshko.tm.api.service;

import ru.tsc.babeshko.tm.api.repository.IRepository;
import ru.tsc.babeshko.tm.model.AbstractModel;

public interface IService<M extends AbstractModel> extends IRepository<M> {

}