package ru.tsc.babeshko.tm.task;

import org.jetbrains.annotations.NotNull;
import ru.tsc.babeshko.tm.component.Server;

public abstract class AbstractServerTask implements Runnable {

    @NotNull
    protected Server server;

    public AbstractServerTask(@NotNull final Server server) {
        this.server = server;
    }

}